import "./App.scss";

function App() {
  return (
    <div className="root">
      <div className="cover">
        <img src="./images/cover-photo.png" alt="" />

        <div className="headerText">
          There is no other <br />
          platforms for you as like
        </div>
        <button className="play" onClick="">
          <img src="./images/play.png" alt="" />
        </button>
        <div className="RoundedRec">TRY NOW</div>
        <div className="underText">* No need to add card details</div>
      </div>
      <div className="section2">
        <div className="section2Photo">
          <img src="./images/section2.png" alt="" />
        </div>
        <div className="section2Desc">
          <div className="newDesign">New Design</div>
          <div className="thereIs">
            There is no other platforms
            <br /> for you as like ....
          </div>
          <div className="paragraph">
            Lorem Ipsum has been the industry's standard dummy text ever since
            the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only
            five centuries, but also the leap into electronic typesetting,
            <br />
            <br /> remaining essentially unchanged. It was popularised in the
            1960s with the release of Letraset sheets containing Lorem Ipsum
            passages, and more recently with desktop publishing
          </div>
        </div>
      </div>
      <div className="section3">
        <div className="mac">
          <img src="./images/macbook.png" alt="" />
        </div>
        <div className="section3Desc">
          <div className="newDesign">New Design</div>
          <div className="responsive">
            Responsive design,just <br />
            need your tap ....
          </div>
          <div className="Desc3">
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia,
          </div>
        </div>
      </div>
      <div className="section4">
        <div className="image">
          <img src="./images/section5Cover.jpg" alt="" />
        </div>
        <ul className="avatar">
          <li>
            <img src="./images/avatar.png" alt="" />
            <h2>Jonathon doe</h2>
            <h6>Co founder</h6>
            <p>
              "Contrary to popular belief, Lorem Ipsum is not simply random
              text. It has roots in a piece of classical Latin literature from
              45 BC,
            </p>
          </li>
          <li>
            <img src="./images/avatar.png" alt="" />
            <h2>Jonathon doe</h2>
            <h6>Co founder</h6>
            <p>
              "Contrary to popular belief, Lorem Ipsum is not simply random
              text. It has roots in a piece of classical Latin literature from
              45 BC,
            </p>
          </li>
          <li>
            <img src="./images/avatar.png" alt="" />
            <h2>Jonathon doe</h2>
            <h6>Co founder</h6>
            <p>
              "Contrary to popular belief, Lorem Ipsum is not simply random
              text. It has roots in a piece of classical Latin literature from
              45 BC,
            </p>
          </li>
        </ul>
        <ul className="partnerLogo">
          <li>
            <img src="./images/partner-logo1.png" alt="" />
          </li>
          <li>
            <img src="./images/partner-logo2.png" alt="" />
          </li>
          <li>
            <img src="./images/partner-logo3.png" alt="" />
          </li>
          <li>
            <img src="./images/partner-logo4.png" alt="" />
          </li>
          <li>
            <img src="./images/partner-logo5.png" alt="" />
          </li>
        </ul>
      </div>
      <div className="section5">
        <div className="section5-content">
          <h4>NEW FEATURES</h4>
          <h3>Some awesome features</h3>
          <h6>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia,
          </h6>
          <img src="./images/tabScreen.png" alt="" />
        </div>
      </div>
      <div className="section6">
        <div className="section6Content">
          <h4>NEW FEATURES</h4>
          <h3>Over 1000 designers are using ...</h3>
          <div className="days30">
            <img src="./images/30days.png" alt="" />
          </div>
          <div>
            <ul>
              <li>
                <input placeholder="FULL NAME" type="text" />
              </li>
              <li>
                <input placeholder="YOUR EMAIL" type="text" />
              </li>
              <li>
                <input placeholder="PASSWORD" type="text" />
              </li>
              <li>
                <button onClick="">TRY NOW</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="section7">
        <div>
          <ul className="footer">
            <li>
              <img src="./images/StartEx.png" alt="" />
              <p>
                "Contrary to popular belief, Lorem Ipsum is not simply random
                text.
              </p>
            </li>
            <li>
              <h2>Support</h2>
              <h4>Help center</h4>
              <h4>get started</h4>
              <h4>contact us</h4>
            </li>
            <li>
              <h2>About US</h2>
              <h4>About us</h4>
              <h4>Terms of use</h4>
              <h4>privacy policy</h4>
            </li>
            <li>
              <h2>Get Newsletter</h2>
              <input type="text" placeholder="EMAIL" />
              <input type="image" alt="submit" src="./images/arrow.png" />

              <div className="social">
                <img src="./images/social.png" alt="" />
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
